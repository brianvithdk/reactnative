import { useEffect, useState } from "react";
import yelp from "../api/yelp";

export default () => {
    const [results, setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');

    const searchApi = async (searchTerm) => {
        //console.log('test api'); // test of Bad Code
        try{
            const response = await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: searchTerm,
                    location: 'los gatos'
                }
            });
            setResults(response.data.businesses);
        }catch(err){
            console.log(err);
            setErrorMessage("Something went wrong");
        }
    };

    // Call searhApi when component is first rendered. 
    // THIS IS BAD CODE... KEEPS LOOPING!
    //searchApi('pasta');

    // Use the following to only render once on startup:
    useEffect(() => {
        searchApi('pasta');
    }, []);

    return [searchApi, results, errorMessage];
};
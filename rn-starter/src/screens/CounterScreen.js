import react, {useState} from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const CounterScreen = () => {

//brug af hook, der opdaterer state ved brug. parameteren i () er start value.
const [counter, setCounter] = useState(0);

    return (
        <View>
            <Button title="Increase" onPress={() => {
                setCounter(counter+1);
            }} />
            <Button title="Decrease" onPress={() => {
                setCounter(counter-1);
            }}/>
            <Text>Current Count: {counter}</Text>
        </View>
    );
};

const styles = StyleSheet.create({});

export default CounterScreen;
import React from "react";
import { View, Text, StyleSheet } from "react-native";

// const ComponentsScreen = function() {...};
const ComponentsScreen = () => {

    const name = "Brian";

    return (
    <View>
        <Text style={styles.textStyle}>Getting started with React Native!</Text>
        <Text style={{fontSize: 20}}>My name is {name}</Text>
    </View>
    );
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 45,
    }
});

export default ComponentsScreen;
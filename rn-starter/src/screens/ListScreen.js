import react from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

const ListScreen = () => {
    const friends = [
        { name: "Friend #1", age: 10 },
        { name: "Friend #2", age: 40 },
        { name: "Friend #3", age: 60 },
        { name: "Friend #4", age: 88 },
        { name: "Friend #5", age: 28 },
        { name: "Friend #6", age: 56 },
        { name: "Friend #7", age: 46 },
        { name: "Friend #8", age: 53 },
        { name: "Friend #9", age: 71 },
        // { name: "Friend #9", key: '9' },
    ];

    return (
        <FlatList 
            // horizontal // keyword kan lave listen horisontal i stedet for vertikal 
            // showsHorizontalScrollIndicator={false} // fjerner scrollbar
            keyExtractor={(friend)=> friend.name} // manuelt add key eller brug extractor - Alle keys skal være unikke
            data={friends} // her vælges hvilken data der skal bruges til FlatListen. Vi bruger vores Array friends
            renderItem={({ item }) => { // itererer over hvert enkelt element i listen
                return ( 
                <Text style={styles.textStyle}>
                    {item.name} - Age: {item.age}
                </Text>
                );
            }}
        />     
    );
};

const styles = StyleSheet.create({
    textStyle: {
        marginVertical: 20,
    }
});

export default ListScreen;
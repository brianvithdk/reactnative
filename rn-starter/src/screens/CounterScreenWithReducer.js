import react, {useReducer} from "react";
import { View, Text, StyleSheet, Button } from "react-native";

const INCREMENT_COUNT = 5;

const reducer = (state, action) => {
    switch(action.type)
    {
        case 'Increment':
            return  {...state, counter: state.counter + action.payload};
        case 'Decrement':
            return  {...state, counter: state.counter - action.payload};
        default:
            return state;
    }
};

const CounterScreenWithReducer = () => {
    const [state, dispatch] = useReducer(reducer, {counter: 0})

    return (
        <View>
            <Button title="Increase" onPress={() => {
                dispatch({type: 'Increment', payload: INCREMENT_COUNT});

            }} />
            <Button title="Decrease" onPress={() => {
                dispatch({type: 'Decrement', payload: INCREMENT_COUNT});
                
            }}/>
            <Text>Current Count: {state.counter}</Text>
        </View>
    );
};

const styles = StyleSheet.create({});

export default CounterScreenWithReducer;
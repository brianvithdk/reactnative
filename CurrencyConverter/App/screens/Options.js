import React from 'react';
import { ScrollView, Linking, Alert, StatusBar } from 'react-native';
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context';
import { Entypo } from '@expo/vector-icons';

import colors from '../constants/colors';
import { RowItem, RowSeparator } from '../components/RowItem';

// asdf://werouiwer.ui


const openURL = (url) => {
    return Linking.openURL(url).catch(() => {
        Alert.alert('Sorry, something went wrong.', 'Please try again later.')
    });
}


export default () => {
return (
    <SafeAreaProvider>
        <SafeAreaView style={{flex: 1}}>
        <StatusBar barStyle="dark-content" backgroundColor={colors.white} />
            <ScrollView>
    
                <RowItem
                    text="Themes"
                    onPress={() => 
                    openURL("https://google.com")
                }
                    rightIcon={
                        <Entypo name ="chevron-right" size={20} color={colors.blue} />}
                />
                <RowSeparator />

                <RowItem
                    text="React Native Basics"
                    onPress={() => alert("todo!")}
                    rightIcon={
                        <Entypo name ="export" size={20} color={colors.blue} />}
                />
                <RowSeparator />
            
                <RowItem
                    text="React Native by Example"
                    onPress={() => alert("todo!")}
                    rightIcon={
                        <Entypo name ="export" size={20} color={colors.blue} />}
                />

            </ScrollView>
        </SafeAreaView>
    </SafeAreaProvider>
    );
}

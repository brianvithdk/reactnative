import React from 'react';
//import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../screens/Home';
import Options from '../screens/Options';
import CurrencyList from '../screens/CurrencyList';
import { ConversionContextProvider } from '../util/ConversionContext';

const MainStack = createStackNavigator();
const MainStackScreen = () => (
    <MainStack.Navigator>
        <MainStack.Screen name = "Home" component={Home} options={{headerShown: false}} />
        <MainStack.Screen name = "Options" component={Options}/>
    </MainStack.Navigator>
);

const ModalStack = createStackNavigator();
const ModalStackScreen = () => (
    <ModalStack.Navigator >
        <ModalStack.Screen 
          screenOptions={{ presentation: "modal" }}
          name="Main"
          component={MainStackScreen}
          options={{headerShown: false}} />
        <ModalStack.Screen name = "CurrencyList" component={CurrencyList} options={({route}) => ({
            title: route.params && route.params.title
        })} />
    </ModalStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <ConversionContextProvider>
            <ModalStackScreen />
        </ConversionContextProvider>
    </NavigationContainer>
)
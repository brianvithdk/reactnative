import axios from "axios";

export default axios.create({
    baseURL: 'https://4955-2a05-f6c7-2955-0-a9e9-1bf6-2bc5-d87.eu.ngrok.io'
});
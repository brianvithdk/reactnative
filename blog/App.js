import React from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Feather, EvilIcons } from '@expo/vector-icons';
import { Provider } from './src/context/BlogContext';
import IndexScreen from './src/screens/IndexScreen';
import ShowScreen from './src/screens/ShowScreen';
import CreateScreen from './src/screens/CreateScreen';
import EditScreen from './src/screens/EditScreen';
 
const Stack = createStackNavigator();
 
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerTitleAlign: 'center'}} >
        <Stack.Screen
          name='Index'
          component={IndexScreen}
          options={({ navigation: { navigate } }) => ({
            headerTitle: 'Blog list',
            headerRight: () => (
              <TouchableOpacity onPress={() => navigate('Create')}>
                <Feather name='plus' size={30} />
              </TouchableOpacity>
            ),
          })}
        />
        <Stack.Screen 
          name='Show'
          component={ShowScreen}
          options={({ navigation: { navigate }, route }) => ({
            headerRight: () => (
              <TouchableOpacity onPress={() => navigate('Edit', {id: route.params.id})}>
                <EvilIcons name='pencil' size={35} />
              </TouchableOpacity>
            )
          })} 
        />
        <Stack.Screen name='Create' component={CreateScreen} />
        <Stack.Screen name='Edit' component={EditScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
 
export default () => {
  return (
    <Provider>
      <App />
    </Provider>
  );
};